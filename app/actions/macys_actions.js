import axios from 'axios';
import { SAVE_STATION, CLEAR_SAVED_STATIONS, REMOVE_STATION, FETCH_CATEGORIES, FETCH_STORIES } from './types';

export const fetchStories = (categoryId, callback) => async dispatch => {
  try {

const options = {
  method: 'GET',
  url: 'https://covid-19-news.p.rapidapi.com/v1/covid',
  params: {q: 'covid', lang: 'en', country: 'US', media: 'True'},
  headers: {
    'x-rapidapi-key': '5ece6bddf0msh523a16ba7afeb28p138ad3jsn2427c9380e5c',
    'x-rapidapi-host': 'covid-19-news.p.rapidapi.com'
  }
};


    let { data } = axios.request(options).then(function (response) {
      console.log(response.data.articles);
      return response.data.articles;
    }).then(function (response) {

      console.log(response)
      dispatch({ type: FETCH_STORIES, payload: response });
      callback();
    }).catch(function (error) {
      console.error(error);
    });

  } catch (error) {
    console.log(error);
  }
};
export const fetchStations = (categoryId, callback) => async dispatch => {
  try {
    // let { data } = await axios({
    //   mode: 'no-cors',

    //   method: 'GET',
    //   url: `http://api.macys.com/v3/catalog/category/${categoryId}/browseproducts?imagewidth=600&imagequality=180`,
    //   headers: {
    //     accept: 'application/json',
    //     'x-macys-webservice-client-id': 'xmj9js4jkdpe1983fmwu98gh'
    //   }
    // });
    // import axios from "axios";

    const options = {
      method: 'GET',
      url: 'https://google-mobility-data.p.rapidapi.com/api/data',
      params: { name: 'United States' },
      headers: {
        'x-rapidapi-key': '5ece6bddf0msh523a16ba7afeb28p138ad3jsn2427c9380e5c',
        'x-rapidapi-host': 'google-mobility-data.p.rapidapi.com'
      }
    };

    let { data } = await axios.request(options).then(function (response) {
      console.log(response.data);
      return response.data;
    }).then(function (response) {

      console.log(response)
      dispatch({ type: FETCH_CATEGORIES, payload: response[0].data[0] });
      callback();
    }).catch(function (error) {
      console.error(error);
    });

  } catch (error) {
    console.log(error);
  }
};

export const saveStation = station => {
  return {
    payload: station,
    type: SAVE_STATION
  };
};

export const clearSavedStations = () => {
  return { type: CLEAR_SAVED_STATIONS };
};

export const removeSavedStation = station => {
  return {
    payload: station,
    type: REMOVE_STATION
  };
};
