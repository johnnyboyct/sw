React Native App
## Dependencies

- react-native & Expo
- react navigation
- redux & react-redux
- redux-persist
- axios

## Getting started

```
git clone https://github.com/mitulsavani/react-native-macysExpress.git

yarn install

expo start

expo ios

expo android
```

